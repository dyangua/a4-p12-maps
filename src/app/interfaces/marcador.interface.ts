export interface Marcador {
    lat: number;
    log: number;
    draggable: boolean;
    titulo: string;
    desc?: string;
}
