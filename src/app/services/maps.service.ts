import { Injectable } from '@angular/core';
import { Marcador } from '../interfaces/marcador.interface';

@Injectable()
export class MapsService {

  marcadores: Marcador[] = [];

  constructor() {
    const nuevoMarcador: Marcador = {
      lat : -0.266373,
      log : -78.538201,
      titulo: 'Plazita',
      draggable: true,
      desc: 'plaza de comidas'
    };

    this.marcadores.push(nuevoMarcador);

  }



  insertarMarcador ( marcador: Marcador) {
    this.marcadores.push(marcador);
    this.guardarMarcadores();
  }

  guardarMarcadores() {
    localStorage.setItem('marcadores',  JSON.stringify( this.marcadores ) );
  }

  cargarMarcadores() {
    if ( localStorage.getItem('marcadores') ) {
      this.marcadores = JSON.parse( localStorage.getItem('marcadores'));
    }else {
      this.marcadores = [];
    }
  }

  borrarMarcador(id: number) {
    this.marcadores.splice(id, 1);
    this.guardarMarcadores();
  }

}
