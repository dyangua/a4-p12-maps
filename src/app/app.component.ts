import { Component } from '@angular/core';
import { MapsService } from './services/maps.service';
import { Marcador } from './interfaces/marcador.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  lat = -0.266373;
  lng = -78.538201;
  zoom = 16;

  marcadorSel: any = null;
  draggable = '1';

  constructor(public _ms: MapsService) {
    this._ms.cargarMarcadores();
  }

  clickMapa( evento ) {
    const nuevoMarcador: Marcador = {
      lat : evento.coords.lat,
      log : evento.coords.lng,
      titulo: 'Sin titulo',
      draggable: true,
      desc: 'plaza de comidas'
    };

    this._ms.insertarMarcador(nuevoMarcador);
    console.log(evento);
  }


  clickMarcador( marcador: Marcador, i: number ) {
    console.log(marcador, i);
    this.marcadorSel = marcador;

    if ( this.marcadorSel.draggable) {
      this.draggable = '1';
    } else {
      this.draggable = '0';
    }

  }


  dragEndMarcador( marcador: Marcador, evento ) {
    console.log(marcador, evento);
    const lat = evento.coords.lat;
    const log = evento.coords.lng;
    marcador.lat = lat;
    marcador.log = log;
  }


  cambiarDraggable() {
    if ( this.draggable === '1') {
      this.marcadorSel.draggable = true;
    } else {
      this.marcadorSel.draggable = false;
    }
  }


}

